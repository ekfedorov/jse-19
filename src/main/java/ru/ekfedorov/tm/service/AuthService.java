package ru.ekfedorov.tm.service;

import ru.ekfedorov.tm.api.service.IAuthService;
import ru.ekfedorov.tm.api.service.IUserService;
import ru.ekfedorov.tm.model.User;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.empty.LoginIsEmptyException;
import ru.ekfedorov.tm.exception.empty.PasswordIsEmptyException;
import ru.ekfedorov.tm.util.HashUtil;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    public User getUser() throws Exception {
        if (!isAuth()) throw new AccessDeniedException();
        return userService.findOneById(userId);
    }

    @Override
    public String getUserId() throws Exception {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void login(final String login, final String password) throws Exception {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void registry(final String login, final String password, final String email) throws Exception {
        userService.create(login, password, email);
    }

}
