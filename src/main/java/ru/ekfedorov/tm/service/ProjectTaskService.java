package ru.ekfedorov.tm.service;

import ru.ekfedorov.tm.api.repository.IProjectRepository;
import ru.ekfedorov.tm.api.repository.ITaskRepository;
import ru.ekfedorov.tm.api.service.IProjectTaskService;
import ru.ekfedorov.tm.exception.empty.ProjectIdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.TaskIdIsEmptyException;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.model.Task;

import static ru.ekfedorov.tm.util.ValidateUtil.*;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskByProject(
            final String projectId, final String taskId
    ) throws Exception {
        if (isEmpty(projectId)) throw new ProjectIdIsEmptyException();
        if (isEmpty(taskId)) throw new TaskIdIsEmptyException();
        return taskRepository.bindTaskByProjectId(projectId, taskId);
    }

    @Override
    public List<Task> findAllByProjectId(
            final String projectId
    ) throws Exception {
        if (isEmpty(projectId)) throw new ProjectIdIsEmptyException();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Project removeProjectById(
            final String projectId
    ) throws Exception {
        if (isEmpty(projectId)) throw new ProjectIdIsEmptyException();
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeOneById(projectId);
    }

    @Override
    public Task unbindTaskFromProject(
            final String taskId
    ) throws Exception {
        if (isEmpty(taskId)) throw new TaskIdIsEmptyException();
        return taskRepository.unbindTaskFromProjectId(taskId);
    }

}
