package ru.ekfedorov.tm.service;

import ru.ekfedorov.tm.api.repository.ITaskRepository;
import ru.ekfedorov.tm.api.service.ITaskService;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.incorrect.IncorrectIndexException;
import ru.ekfedorov.tm.exception.empty.DescriptionIsEmptyException;
import ru.ekfedorov.tm.exception.empty.IdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.NameIsEmptyException;
import ru.ekfedorov.tm.exception.system.NullTaskException;
import ru.ekfedorov.tm.model.Task;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;
import static ru.ekfedorov.tm.util.ValidateUtil.notNullOrLessZero;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(
            final String name, final String description
    ) throws Exception {
        if (isEmpty(name)) throw new NameIsEmptyException();
        if (isEmpty(description)) throw new DescriptionIsEmptyException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public Task changeTaskStatusById(
            final String id, final Status status
    ) throws Exception {
        if (isEmpty(id)) throw new IdIsEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new NullTaskException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(
            final Integer index, final Status status
    ) throws Exception {
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        final Task task = findOneByIndex(index);
        if (task == null) throw new NullTaskException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByName(
            final String name, final Status status
    ) throws Exception {
        if (isEmpty(name)) throw new NameIsEmptyException();
        final Task task = findOneByName(name);
        if (task == null) throw new NullTaskException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task findOneByIndex(final Integer index) throws Exception {
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task findOneByName(final String name) throws Exception {
        if (isEmpty(name)) throw new NameIsEmptyException();
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task finishTaskById(final String id) throws Exception {
        if (isEmpty(id)) throw new IdIsEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new NullTaskException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByIndex(final Integer index) throws Exception {
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        final Task task = findOneByIndex(index);
        if (task == null) throw new NullTaskException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByName(final String name) throws Exception {
        if (isEmpty(name)) throw new NameIsEmptyException();
        final Task task = findOneByName(name);
        if (task == null) throw new NullTaskException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task removeOneByIndex(final Integer index) throws Exception {
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        return taskRepository.removeOneByIndex(index);
    }

    @Override
    public Task removeOneByName(final String name) throws Exception {
        if (isEmpty(name)) throw new NameIsEmptyException();
        return taskRepository.removeOneByName(name);
    }

    @Override
    public Task startTaskById(final String id) throws Exception {
        if (isEmpty(id)) throw new IdIsEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new NullTaskException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(final Integer index) throws Exception {
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        final Task task = findOneByIndex(index);
        if (task == null) throw new NullTaskException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(final String name) throws Exception {
        if (isEmpty(name)) return null;
        final Task task = findOneByName(name);
        if (task == null) throw new NullTaskException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task updateTaskById(
            final String id, final String name, final String description
    ) throws Exception {
        if (isEmpty(id)) throw new IdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new NullTaskException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(
            final Integer index, final String name, final String description
    ) throws Exception {
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        if (isEmpty(name)) throw new NameIsEmptyException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new NullTaskException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

}
