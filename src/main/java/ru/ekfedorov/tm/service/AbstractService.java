package ru.ekfedorov.tm.service;

import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.api.IService;
import ru.ekfedorov.tm.exception.empty.IdIsEmptyException;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public class AbstractService<E extends AbstractEntity> implements IService<E> {

    private final IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public E add(final E entity) throws Exception {
        if (entity == null) throw new NullObjectException();
        return repository.add(entity);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator) throws Exception {
        if (comparator == null) throw new NullComparatorException();
        return repository.findAll(comparator);
    }

    @Override
    public E findOneById(final String id) throws Exception {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return repository.findOneById(id);
    }

    @Override
    public E remove(final E entity) throws Exception {
        if (entity == null) throw new NullObjectException();
        return repository.remove(entity);
    }

    @Override
    public E removeOneById(final String id) throws Exception {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return repository.removeOneById(id);
    }

}
