package ru.ekfedorov.tm.util;

public interface ValidateUtil {

    static boolean isEmpty(final String value) {
        return value == null || value.isEmpty();
    }

    static boolean notNullOrLessZero(final Integer value) {
        return value == null || value < 0;
    }

}
