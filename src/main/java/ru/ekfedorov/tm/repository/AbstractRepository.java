package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public E add(final E entity) {
        list.add(entity);
        return entity;
    }

    @Override
    public void clear() {
    }

    @Override
    public List<E> findAll() {
        return list;
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator) {
        final List<E> entities = new ArrayList<>(list);
        entities.sort(comparator);
        return entities;
    }

    @Override
    public E findOneById(final String id) {
        for (E entity : list) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E remove(final E entity) {
        list.remove(entity);
        return entity;
    }

    @Override
    public E removeOneById(final String id) {
        final E entity = findOneById(id);
        if (entity == null) return null;
        return remove(entity);
    }

}
