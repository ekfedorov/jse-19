package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.api.repository.IProjectRepository;
import ru.ekfedorov.tm.model.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index >= list.size()) return null;
        return list.get(index);
    }

    @Override
    public Project findOneByName(final String name) {
        for (final Project project : list) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Override
    public Project removeOneByName(final String name) {
        final Project project = findOneByName(name);
        if (project == null) return null;
        remove(project);
        return project;
    }

}
