package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.api.repository.IUserRepository;
import ru.ekfedorov.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        for (User user : list) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public Boolean isLoginExist(final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

}
