package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.api.repository.ITaskRepository;
import ru.ekfedorov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public Task bindTaskByProjectId(
            final String projectId, final String taskId
    ) {
        final Task task = findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (isEmpty(projectId)) return null;
        final List<Task> list1 = new ArrayList<>();
        for (Task task : list) {
            if (projectId.equals(task.getProjectId())) list1.add(task);
        }
        return list1;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index >= list.size()) return null;
        return list.get(index);
    }

    @Override
    public Task findOneByName(final String name) {
        for (final Task task : list) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public void removeAllByProjectId(final String projectId) {
        if (isEmpty(projectId)) return;
        list.removeIf(task -> projectId.equals(task.getProjectId()));
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task unbindTaskFromProjectId(final String taskId) {
        final Task task = findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

}
