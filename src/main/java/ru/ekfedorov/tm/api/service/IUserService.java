package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.api.IService;
import ru.ekfedorov.tm.model.User;
import ru.ekfedorov.tm.enumerated.Role;

public interface IUserService extends IService<User> {

    User create(String login, String password) throws Exception;

    void create(String login, String password, String email) throws Exception;

    void create(String login, String password, Role role) throws Exception;

    User findByLogin(String login) throws Exception;

    boolean isLoginExist(String login) throws Exception;

    User removeByLogin(String login) throws Exception;

    void setPassword(String userId, String password) throws Exception;

    void userUpdate(
            String userId,
            String firstName,
            String lastName,
            String middleName
    ) throws Exception;

}
