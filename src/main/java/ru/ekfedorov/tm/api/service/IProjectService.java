package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.api.IService;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.model.Project;

public interface IProjectService extends IService<Project> {

    Project add(String name, String description) throws Exception;

    Project changeProjectStatusById(String id, Status status) throws Exception;

    Project changeProjectStatusByIndex(Integer index, Status status) throws Exception;

    Project changeProjectStatusByName(String name, Status status) throws Exception;

    Project findOneByIndex(Integer index) throws Exception;

    Project findOneByName(String name) throws Exception;

    Project finishProjectById(String id) throws Exception;

    Project finishProjectByIndex(Integer index) throws Exception;

    Project finishProjectByName(String name) throws Exception;

    Project removeOneByIndex(Integer index) throws Exception;

    Project removeOneByName(String name) throws Exception;

    Project startProjectById(String id) throws Exception;

    Project startProjectByIndex(Integer index) throws Exception;

    Project startProjectByName(String name) throws Exception;

    Project updateProjectById(String id, String name, String description) throws Exception;

    Project updateProjectByIndex(Integer index, String name, String description) throws Exception;

}
