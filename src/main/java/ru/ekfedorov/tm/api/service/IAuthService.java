package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.model.User;

public interface IAuthService {

    User getUser() throws Exception;

    String getUserId() throws Exception;

    IUserService getUserService();

    boolean isAuth();

    void login(String login, String password) throws Exception;

    void logout();

    void registry(String login, String password, String email) throws Exception;

}
