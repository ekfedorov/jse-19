package ru.ekfedorov.tm.api.repository;

import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    Boolean isLoginExist(String login);

    User removeByLogin(String login);

}
