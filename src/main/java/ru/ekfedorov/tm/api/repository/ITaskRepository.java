package ru.ekfedorov.tm.api.repository;

import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    Task bindTaskByProjectId(String projectId, String taskId);

    List<Task> findAllByProjectId(String projectId);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    void removeAllByProjectId(String projectId);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task unbindTaskFromProjectId(String taskId);

}