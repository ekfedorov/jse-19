package ru.ekfedorov.tm.enumerated;

public enum Role {

    ADMIN("administrator"),
    USER("user");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
