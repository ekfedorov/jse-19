package ru.ekfedorov.tm.model;

import ru.ekfedorov.tm.api.entity.IWBS;
import ru.ekfedorov.tm.enumerated.Status;

import java.util.Date;

public final class Project extends AbstractEntity implements IWBS {

    private Date created = new Date();

    private Date dateFinish;

    private Date dateStart;

    private String description = "";

    private String name = "";

    private Status status = Status.NOT_STARTED;

    public Project() {
    }

    public Project(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(final Date created) {
        this.created = created;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(final Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(final Date dateStart) {
        this.dateStart = dateStart;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return id + ": " + name + " | " + status.getDisplayName();
    }

}
