package ru.ekfedorov.tm.command.system;

import ru.ekfedorov.tm.command.AbstractCommand;
import ru.ekfedorov.tm.util.NumberUtil;

public class InfoCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-i";
    }

    @Override
    public String description() {
        return "Show system info.";
    }

    @Override
    public void execute() {
        System.out.println("[SYSTEM INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        final String processorMsg = "Available processors: " + processors;
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryMsg = "Free memory: " + NumberUtil.format(freeMemory);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.format(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        final String maxMemoryMsg = "Maximum memory: " + maxMemoryValue;
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryMsg = "Total memory available to JVM: " + NumberUtil.format(totalMemory);
        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryMsg = "Used memory by JVM: " + NumberUtil.format(usedMemory);
        System.out.println(processorMsg);
        System.out.println(freeMemoryMsg);
        System.out.println(maxMemoryMsg);
        System.out.println(totalMemoryMsg);
        System.out.println(usedMemoryMsg);
    }

    @Override
    public String name() {
        return "info";
    }

}
