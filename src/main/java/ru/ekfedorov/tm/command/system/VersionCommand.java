package ru.ekfedorov.tm.command.system;

import ru.ekfedorov.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String description() {
        return "Display program version.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.14.0");
    }

    @Override
    public String name() {
        return "version";
    }

}
