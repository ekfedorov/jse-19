package ru.ekfedorov.tm.command.project;

import ru.ekfedorov.tm.api.service.IProjectService;
import ru.ekfedorov.tm.command.AbstractProjectCommand;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.system.NullProjectException;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change project status by name.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final IProjectService projectService = serviceLocator.getProjectService();
        final Project project = projectService.findOneByName(name);
        if (project == null) throw new NullProjectException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project projectUpdated = projectService.changeProjectStatusByName(name, status);
        if (projectUpdated == null) throw new NullProjectException();
    }

    @Override
    public String name() {
        return "change-project-status-by-name";
    }

}
