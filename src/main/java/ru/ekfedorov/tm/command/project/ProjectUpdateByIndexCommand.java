package ru.ekfedorov.tm.command.project;

import ru.ekfedorov.tm.api.service.IProjectService;
import ru.ekfedorov.tm.command.AbstractProjectCommand;
import ru.ekfedorov.tm.exception.system.NullProjectException;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.util.TerminalUtil;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update project by index.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final IProjectService projectService = serviceLocator.getProjectService();
        final Project project = projectService.findOneByIndex(index);
        if (project == null) throw new NullProjectException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectByIndex(index, name, description);
        if (projectUpdated == null) throw new NullProjectException();
    }

    @Override
    public String name() {
        return "project-update-by-index";
    }

}
