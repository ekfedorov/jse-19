package ru.ekfedorov.tm.command.task;

import ru.ekfedorov.tm.command.AbstractTaskCommand;
import ru.ekfedorov.tm.exception.system.NullTaskException;
import ru.ekfedorov.tm.model.Task;
import ru.ekfedorov.tm.util.TerminalUtil;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        String name = TerminalUtil.nextLine();
        if (isEmpty(name)) name = "task";
        System.out.println("ENTER DESCRIPTION:");
        String description = TerminalUtil.nextLine();
        if (isEmpty(description)) description = "without description";
        final Task task = serviceLocator.getTaskService().add(name, description);
        if (task == null) throw new NullTaskException();
        System.out.println();
    }

    @Override
    public String name() {
        return "task-create";
    }

}
