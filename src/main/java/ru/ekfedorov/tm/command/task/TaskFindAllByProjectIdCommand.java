package ru.ekfedorov.tm.command.task;

import ru.ekfedorov.tm.api.service.IProjectTaskService;
import ru.ekfedorov.tm.command.AbstractTaskCommand;
import ru.ekfedorov.tm.model.Task;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.List;

public class TaskFindAllByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Find al lby project id.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST BY PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final IProjectTaskService projectTaskService = serviceLocator.getProjectTaskService();
        final List<Task> tasks = projectTaskService.findAllByProjectId(id);
        if (tasks.isEmpty()) {
            System.out.println("--- There are no tasks ---");
            return;
        }
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    public String name() {
        return "find-all-by-project-id";
    }

}
